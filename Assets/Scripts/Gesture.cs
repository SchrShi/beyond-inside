﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using Mediapipe.HandTracking;

public class Gesture : MonoBehaviour
{
    public static Gesture INSTANCE;
    private Camera cam;
    private Hand hand;
    private void Awake()
    {
        INSTANCE = this;
        hand = FindObjectOfType<Process>().current_hand;
    }

    private void FixedUpdate()
    {
        if (isAgree(hand))
        {
            ScreenLog.swordDebug = "AGREE";
        }
        else if (isSpiderMan(hand))
        {
            ScreenLog.swordDebug = "SPIDER MAN";
        }
        else
        {
            ScreenLog.swordDebug = "NONE";
        }
    }

    public float angleIndexWithHand(Hand hand)
    {
        return Vector3.Angle(hand.GetLandmark(5) - hand.GetLandmark(0), hand.GetLandmark(8) - hand.GetLandmark(5));
    }


    public bool isAgree(Hand hand)
    {
        if (angleIndexWithHand(hand) > 20 && angleFinger(hand, 5) > 20 && angleBig(hand) > 20 &&
        isHigerThan(hand, 8, 4, 0) && isHigerThan(hand, 12, 8, Vector3.Distance(cam.WorldToScreenPoint(hand.GetLandmark(8)), cam.WorldToScreenPoint(hand.GetLandmark(6)))))
        {
            return true;
        }
        return false;
    }

    public bool isSpiderMan(Hand hand)
    {
        if (angleFingerStraight(hand, 1) && angleFingerStraight(hand, 5) && angleFingerStraight(hand, 17) &&
        isHigerThan(hand, 10, 12, 0) && isHigerThan(hand, 14, 16, 0))
        {
            return true;
        }
        return false;
    }


    public bool isHigerThan(Hand hand, int landmark1, int landmark2, float distance)
    {
        return cam.WorldToScreenPoint(hand.GetLandmark(landmark1)).y - cam.WorldToScreenPoint(hand.GetLandmark(landmark2)).y > distance;
    }

    public bool angleFingerStraight(Hand hand, int fingerWirst)
    {
        return Vector3.Angle(hand.GetLandmark(fingerWirst + 1) - hand.GetLandmark(fingerWirst), hand.GetLandmark(fingerWirst + 2) - hand.GetLandmark(fingerWirst + 1)) < 20;
    }

    public float angleFinger(Hand hand, int fingerWirst)
    {
        return Vector3.Angle(hand.GetLandmark(fingerWirst + 1) - hand.GetLandmark(fingerWirst), hand.GetLandmark(fingerWirst + 3) - hand.GetLandmark(fingerWirst + 1));
    }

    public float angleBig(Hand hand)
    {
        return Vector3.Angle(hand.GetLandmark(3) - hand.GetLandmark(2), hand.GetLandmark(4) - hand.GetLandmark(3));
    }


}

