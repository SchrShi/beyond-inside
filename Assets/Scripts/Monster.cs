﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Monster : Person
{
    [Header("Сколько опыта падает со смерти монстра. Целое число.")]
    public int dropExpirence;

    public void SpawnMob()
    {
        ARRaycastManager ARRaycastManagerScript = FindObjectOfType<ARRaycastManager>();
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        ARRaycastManagerScript.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        if (hits.Count > 0)
        {
            var spawnMob = Instantiate(gameObject);
            spawnMob.transform.position = hits[0].pose.position;
            spawnMob.transform.rotation = hits[0].pose.rotation;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Weapon")
        {
            currentHealth -= collision.gameObject.GetComponent<Sword>().strange;
            if(currentHealth <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

}
