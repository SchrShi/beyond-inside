﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    [Header("Ввести значения персонажа в виде типа float.")]
    [Header("Поглощение урона(защита): 0.1 - 10%, 1 = 100%")]
    public float defence;
    [Header("Сопротивление оглушению.")]
    public float resistance;
    [Header("Сила, сколько снимает урона при 1 ударе.")]
    public float strange;
    [Header("Количество жизни.")]
    public float health;
    [NonSerialized]public float currentHealth;

    GameManager manager = new GameManager();

    void Start()
    {
        currentHealth = health;
    }


    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Spear")
        {
            isDamage(manager.sword.GetComponent<Sword>().strange);
        }
    }

    public void isDamage(float damage) {
        if (defence > 0) currentHealth -= (damage - (damage * defence));
        else currentHealth -= damage;
        if (currentHealth <= 0) isDeath();
    }

    public virtual void isDeath() { }
}
