﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseObject : MonoBehaviour
{
    private Button button;
    public GameObject chooseObject;

    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ChooseObjectFunction);
    }

    // Update is called once per frame
     void ChooseObjectFunction()
    {
        Instantiate(chooseObject);
    }
}
