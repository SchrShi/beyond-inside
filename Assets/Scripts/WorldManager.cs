﻿using Mediapipe.HandTracking.ARCore;
using UnityEngine;
using UnityEngine.UI;

public class WorldManager : MonoBehaviour {

    [SerializeField]
    private GameObject game = null, player_camera = null;
    private bool fps = false;
    [SerializeField] private GameObject log;
    [SerializeField]private Text button_text;

    public void ActiveGame() {
        game.transform.localPosition = player_camera.transform.localPosition - new Vector3(0, 0.1f, 0);
        // game.transform.forward = (new Vector3(player_camera.transform.forward.x, 0, player_camera.transform.forward.z)).normalized;
        game.SetActive(true);
    }

    public void ShowFPS()
    {
        if (fps)
        {
            fps = false;
            button_text.text = "FPS ON";

        }
        else {
            fps = true;
            button_text.text = "FPS OFF";
        }
        log.SetActive(fps);
    }

    public void SpawnSword()
    {
        GameManager manager = game.GetComponent<GameManager>();
        manager.sword.GetComponent<Sword>().SpawnDestroySword();
    }

    public void SpawnMob()
    {
        GameManager manager = game.GetComponent<GameManager>();
        manager.mob.GetComponent<Monster>().SpawnMob();
    }

}
