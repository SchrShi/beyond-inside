﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : Person
{
    public int expirence = 0;
    public int level = 1;
    public int expirenceMax = 10;


    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Handle")
        {

        }
    }

    public void LevelUp()
    {
        if (expirence == expirenceMax)
        {
            level++;
            expirence = 0;
            expirenceMax = expirenceMax + (level * 10);
        }
    }

    public void KillMonster(Monster monster)
    {
        expirence += monster.dropExpirence;
    }


}
