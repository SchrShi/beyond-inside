![Beyond Inside Logo](https://cdn.discordapp.com/attachments/660979317978955779/733769095899775066/imgonline-com-ua-Transparent-backgr-BjwWWmkp8giJ1.png "Beyond Inside Logo")
# AR RPG Game


### "Что это?"
>>>
Добро пожаловать в репозиторий разработки Beyond Inside AR
Здесь я(Пока что один) разрабатываю игру с помощью таких технологий, как:
- Android NDK
- Unity Engine
- ARCore
- MediaPipe

На данный момент игра не вышла даже в альфа-версию, но вы можете посмотреть код. Не забывайте читать лицензию перед использованием!
>>>

### "Как я могу помочь?"
>>>
На данный момент ты можешь помочь мне, делая PullRequest в данный репозиторий. Чуть позже опубликую ссылку на kickstarter.
>>>

### Ссылки
[Блог ВКонтакте](https://vk.com/beyondinside_game)


